$Id: LISEZMOI.txt,v 1.4 2010/30/12 12:00:00 bthomas Exp $


-----------------------------
Dolist.net webservice Module
-----------------------------
by Benjamin Thomas, benjamin.thomas@alterway.fr

With this module you can connect your drupal users information and your dolist.net contacts information via content_profile.


 Installation: 
---------------
 * Copy the module's directory to your modules directory and activate the module.
 
 
 Usage:
--------
 * There will be a new menu tab in your content type "profile".
 * Once the module is activated and profile is set as a content_profile, there will be a new tab "Dolist synchronisation" in 
   "manage fields" of the content type "profile".
 * This tab gives you access to the mapping interface of the module. You need to specify the url of the webservice (if different
   from the default), and your idList. Your fields Content_profile are then listed and you can assign to each field the dolist
   field of your choice dolist (write "Aucun" or leave blank to not use). Use interest id to map interests. Note however that 
   certain elements require particular values: msgformatid, salutations , countries and birthdate.
 
 * Valid values for msgformatid are: Texte, HTML, AOL
 * Valid values for salutations are: Madame, Mademoiselle, Monsieur, Docteur, Professeur, Maître, Madam, Miss, Mister, Doctor, 
   Professor, Master, Señora, Señorita, Señor, Doctor, Professor, Maestro, Mevrouw, Juffrouw, Mijnheer, Signor, Signora, 
   Signorina, Herr, Frau, Fräulein
 * Valid values for countries are: FRANCE, BELGIQUE, PAYS-BAS, ALLEMAGNE, ITALIE, ROYAUME UNI, IRLANDE, DANEMARK, GRECE, PORTUGAL,
   ESPAGNE, CANARIES, CEUTA ET MELILLA, LUXEMBOURG, ISLANDE, ILES FEROE, NORVEGE, SUEDE, FINLANDE, SUISSE, LIECHTENSTEIN, 
   AUTRICHE, ANDORRE, GIBRALTAR, VATICAN, MALTE, YOUGOSLAVIE, TURQUIE, ESTONIE, LETTONIE, LITUANIE, C.E.I., POLOGNE, REPUBLIQUE
   TCHEQUE, SLOVAQUIE, HONGRIE, ROUMANIE, BULGARIE, ALBANIE, UKRAINE, BIELORUSSIE, MOLDAVIE, RUSSIE, GEORGIE, ARMENIE, 
   AZERBAIDJAN, KAZAKHSTAN, TURKMENISTAN, OUZBEKISTAN, TADJIKISTAN, KIRGUIZISTAN, SLOVENIE, CROATIE, BOSNIE HERZEGOVINE, SERBIE
   ET MONTENEGRO, MACEDOINE, MAROC, ALGERIE, TUNISIE, LIBYE, EGYPTE, SOUDAN, MAURITANIE, MALI, BURKINA FASO, NIGER, TCHAD, CAP
   VERT, SENEGAL, GAMBIE, GUINEE BISSAU, GUINEE, SIERRA LEONE, LIBERIA, COTE D'IVOIRE, GHANA, TOGO, BENIN, NIGERIA, CAMEROUN,
   REPUBLIQUE CENTRAFRICAINE, GUINEE EQUATORIALE, SAO TOME ET PRINCIPE, GABON, CONGO, ZAIRE, RWANDA, BURUNDI, STE HELENE ET
   TRISTAN DA CUNHA, ANGOLA, ETHIOPIE, ERYTHREE, DJIBOUTI, SOMALIE, KENYA, OUGANDA, TANZANIE, ILES SEYCHELLES, ARCHIPEL DES
   CHAGOS, MOZAMBIQUE, MADAGASCAR, LA REUNION, MAURITIUS, COMORES, MAYOTTE, ZAMBIE, ZIMBABWE, MALAWI, AFRIQUE DU SUD, NAMIBIE,
   BOTSWANA, SWAZILAND, LESOTHO, USA, CANADA, GROENLAND, SAINT PIERRE ET MIQUELON, MEXIQUE, BERMUDES, GUATEMALA, BELIZE, HONDURAS,
   SALVADOR, NICARAGUA, COSTA RICA, PANAMA, ANGUILLA, CUBA, SAINT KITTS ET NEVIS, HAITI, BAHAMAS, TURKS ET CAICOS, PUERTO RICO,
   REPUBLIQUE DOMINICAINE, GUADELOUPE, ANTIGUA BARBUDA, DOMINIQUE, ILES VIERGES DU R.U. ET MONTSERRAT, MARTINIQUE, ILES CAYMAN,
   JAMAIQUE, SAINTE LUCIE, SAINT VINCENT ET GRENADINES, LA BARBADE, TRINIDAD ET TOBAGO, GRENADE, ARUBA, ANTILLES NEERLANDAISES, 
   COLOMBIE, VENEZUELA, GUYANA, SURINAME, GUYANE FRANCAISE, EQUATEUR, PEROU, BRESIL, CHILI, BOLIVIE, PARAGUAY, URUGUAY, ARGENTINE,
   FALKLANDS, CHYPRE, LIBAN, SYRIE, IRAK, IRAN, ISRAEL, JORDANIE, ARABIE SAOUDITE, KOWEIT, BAHREIN, QATAR, DUBAI, EMIRATS ARABES
   UNIS, OMAN, YEMEN, AFGHANISTAN, PAKISTAN, INDE, BANGLADESH, ILES MALDIVES, SRI LANKA, NEPAL, BHOUTAN, MYANMAR (EX BIRMANIE), 
   THAILANDE, LAOS, VIETNAM, KAMPUCHEA, INDONESIE, MALAISIE, BRUNEI, SINGAPOUR, PHILIPPINES, MONGOLIE, CHINE, COREE DU NORD, COREE
   DU SUD, JAPON, TAIWAN, HONG KONG, MACAO, AUSTRALIE, PAPOUASIE NOUVELLE GUINEE, OCEANIE AUSTRALIENNE, NAURU, NOUVELLE ZELANDE,
   ILES SALOMON, TUVALU, NOUVELLE CALEDONIE, WALLIS ET FUTUNA, KIRIBATI, ILES PITCAIRN, FIJI, VANUATU, TONGA, SAMOA OCCIDENTALES,
   POLYNESIE FRANCAISE, FED. DES ETATS DE MICRONESIE, ILES MARSHALL, MONACO, Autres pays				
 * Accepted formats for "birthdate" are: JJ/MM/AAAA (you can choose other separators), example : 23-12-1993


 Warning:
----------
 * This module requires the PHP SOAP service to be activated.
 * The status field and the interests should be associated with a checkbox, returning the keys 0 for "unsubscribed" or 1 for 
   "subscribed" to the database.
 * Before you can access the mapping, at least one content_profile node must be created.


 Tip :
-------
 * Read the content_profile readme.
 
 
 Notes :
-------------
 * Dolist.net unsubcribe URL via drupal is
   [website root]/admin/content/node-type/profile/unsubscribe/dolist?u=[Dolist.net unsubcribe URL]






